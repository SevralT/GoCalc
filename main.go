// Package name
package main

// Import packages
import (
	"fmt"
)

// Variables
var a, b, c float64
var oper string

// Main functions
func main() {
	fmt.Print("Enter first number: ")
	fmt.Scan(&a)
	fmt.Print("Enter second number: ")
	fmt.Scan(&b)
	fmt.Print("Choose operation (+, -, *, /): ")
	fmt.Scan(&oper)

	switch oper {
	case "+":
		c = Add(a, b)
	case "-":
		c = Sub(a, b)
	case "*":
		c = Multi(a, b)
	case "/":
		c = Div(a, b)
	}
	fmt.Println("Your result:", c)
}

//Operations logic
func Add(a float64, b float64) float64 {
	return a + b
}

func Sub(a float64, b float64) float64 {
	return a - b
}

func Multi(a float64, b float64) float64 {
	return a * b
}

func Div(a float64, b float64) float64 {
	return a / b
}
